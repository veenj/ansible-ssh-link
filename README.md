# ansible-ssh-link

Setup remote ssh keys and copy them to targets.

## Configuration

See `defaults/main.yml` for configuration options.

The role should be run in `serial: 1` mode, otherwise several hosts could modify
the same `authorized_keys` in parallel leading to race conditions.

### Custom SSH servers

For servers which are not a general SSH server (git, rsync), there is also a way
of configuring only the client to be able to connect without any manual setup.
Use this mode by setting `ssh_link_client_only` to `True`. In this case, specify
a hostname or IP address as the `host` field, as such a host is not considered
to be part of an ansible inventory.

This setup is much less preferred. No IP addresses and hostnames will be
gathered from the ansible facts and added to the known\_hosts. Also this is less
secure as the server's ssh key obtained via `ssh-keyscan` is trusted without
further verification (TOFU model). Manually verify the keys via the way the
server publishes its key information!

